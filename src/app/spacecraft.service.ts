// Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class SpaceCraftService {
  Search_URL=  environment.stapiAPI;

  constructor(private httpClient: HttpClient) {}

  public getData(pageNumber: number) {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        Accept: '*/*',
      }),
    };
    return this.httpClient.get(
      this.Search_URL+'?pageNumber='+pageNumber,
      options
    );
  }

  public create(formData: any) {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache',
        Accept: '*/*',
      }),
    };
    return this.httpClient.post(this.Search_URL, formData, options);
  }
}
