import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpacecraftsSearchComponent } from './spacecrafts-search.component';

describe('SpacecraftsSearchComponent', () => {
  let component: SpacecraftsSearchComponent;
  let fixture: ComponentFixture<SpacecraftsSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpacecraftsSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpacecraftsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
