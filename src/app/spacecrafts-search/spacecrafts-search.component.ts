import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { of, Subject, takeUntil } from 'rxjs';
import { SpaceCraftService } from '../spacecraft.service';

@Component({
  selector: 'app-spacecrafts-search',
  templateUrl: './spacecrafts-search.component.html',
  styleUrls: ['./spacecrafts-search.component.scss'],
})
export class SpacecraftsSearchComponent implements OnInit {
  private _subject$: Subject<boolean> = new Subject();
  searchForm = new FormGroup({
    title: new FormControl(''),
  });
  spacecrafts = [];
  SuccessfulSearch: boolean = false;
  pageSize: number = 0;
  totalElements: number = 0;
  skip = 0;
  pageNumber: number = 0;

  constructor(private spaceCraftService: SpaceCraftService) {}

  ngOnInit(): void {}

  public saveForm(searchFormValue: any) {
    let formData = `name=${searchFormValue.title}`;
    this.spaceCraftService
      .create(formData)
      .pipe(takeUntil(this._subject$))
      .subscribe((res: any) => {
        this.skip = 0;
        localStorage.setItem('spacecrafts', JSON.stringify([res]));
        this.SuccessfulSearch = true;
        this.spacecrafts = res.spacecrafts;
        this.totalElements = res.page.totalElements;
        this.spacecrafts.length = res.page.totalElements;
        this.pageSize = res.page.pageSize;
        this.pageNumber = res.page.pageNumber;
      });
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.pageNumber = this.skip / event.take;
    let storedData = JSON.parse(localStorage.getItem('spacecrafts') || '');
    let pageExists = storedData.findIndex(
      (el: any) => el.page.pageNumber == this.pageNumber
    );
    if (pageExists == -1) {
      this.spaceCraftService
        .getData(this.pageNumber)
        .pipe(takeUntil(this._subject$))
        .subscribe((data: any) => {
          storedData.push(data);
          localStorage.setItem('spacecrafts', JSON.stringify(storedData));
          for (
            let i = this.skip;
            i < this.skip + data.page.numberOfElements;
            i++
          ) {
            this.spacecrafts[i] = data.spacecrafts[i - this.skip] as never;
          }
          this.spacecrafts.slice(this.skip, this.skip + this.pageSize);
          this.spacecrafts = [...this.spacecrafts];
        });
    }
  }

  ngOnDestroy(): void {
    this._subject$.unsubscribe();
  }
}
