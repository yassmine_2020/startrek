import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpacecraftsSearchComponent } from './spacecrafts-search/spacecrafts-search.component';
import { LabelModule } from "@progress/kendo-angular-label";
import { InputsModule } from "@progress/kendo-angular-inputs";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SpaceCraftService } from './spacecraft.service';
import { GridModule } from "@progress/kendo-angular-grid";
import { HttpClientModule } from "@angular/common/http";
    
@NgModule({
  declarations: [
    AppComponent,
    SpacecraftsSearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LabelModule,
    ReactiveFormsModule,
    InputsModule,
    ButtonsModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [SpaceCraftService],
  bootstrap: [AppComponent]
})
export class AppModule { }
